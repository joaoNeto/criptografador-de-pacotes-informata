$(document).ready(function(){

      $(".nome-pacote, .nome-versao-banco, .nome-versao-reversao").keyup(function(){
        var nome_pacote = $(".nome-pacote");
        var versao_apl = $(".nome-versao-banco");
        var versao_rev = $(".nome-versao-reversao");

        if((nome_pacote.val() !== "") && (versao_apl.val() !== "") && (versao_rev.val() !== ""))
          $(".gerar-pacote").removeAttr('disabled').css({'background-color': '#005d94',opacity:'1'});
        else
          $(".gerar-pacote").attr('disabled','disabled').css({'background-color': '#999',opacity:'0.5'});
      });
    $(".rodape #nome-versao-banco").mask('00.00.00.00');
    $(".rodape #nome-versao-reversao").mask('00.00.00.00');

    // gerar pacote pelo modal
    $(".modal-gerar-pacote #gerar-pacote").click(function(){
      $(this).val("gerando pacote...");
      $(this).prop("disabled",true);

      var arrDados = {
                      "nome_pacote" : $(".modal-gerar-pacote #nome-pacote").val(), 
                      "versao_banco" : $(".modal-gerar-pacote #nome-versao-banco").val(), 
                      "versao_reversao" : $(".modal-gerar-pacote #nome-versao-reversao").val()
                     };

      $.post('gerenciar-arquivo.php?param=gerar-pasta-pacote', arrDados, function(caminhoArquivo){

        $(".modal-gerar-pacote #gerar-pacote").val("Gerar pacote");
        $(".modal-gerar-pacote #gerar-pacote").prop("disabled",false);
        $(".baixar-file").html("<a class='baixar-file' href='out/meus pacotes dinamicos/"+arrDados.nome_pacote+"_"+arrDados.versao_banco+".zip'> <input type='button' value='baixar "+arrDados.nome_pacote+"_"+arrDados.versao_banco+".zip' id='gerar-pacote' class='voltar-botao-pack-dinamic' > </a>");
        $(".voltar-botao-pack-dinamic").click(function(){
          $(".baixar-file").html("<input type='button' value='Gerar pacote' id='gerar-pacote'>");
        });
        $(".modal-gerar-pacote #nome-pacote").val("");
        $(".modal-gerar-pacote #nome-versao-banco").val("");
        $(".modal-gerar-pacote #nome-versao-reversao").val("");

      });

    });

    $(".icone-duvida-informativo.abrir-modal").click(function(){
      $(this).addClass("fechar-modal");
      $(this).removeClass("abrir-modal");
      $(".modal-gerar-pacote").show();
    });

    $(".fechar-modal").click(function(){
      $(".icone-duvida-informativo.fechar-modal").removeClass("fechar-modal");
      $(".icone-duvida-informativo.fechar-modal").addClass("abrir-modal");
      $(".modal-gerar-pacote").hide();
    });

    $("#subir-arquivo-tortoise").click(function(){
        $("#formulario").hide();
        $("#urlRepo").show();           
    });

    $(".esconderUrlRepo").click(function(){
        $("#formulario").show();
        $("#urlRepo").hide();                       
    });

    $(".excluir-todos-arquivos").click(function(){
      var excluir = confirm("Deseja refazer o pacote?");
      if(excluir == 1){
        $.ajax({
            url: 'gerenciar-arquivo.php?param=deletar-todos-arquivos',
            success: function (data) {
             angular.element('#myCtrl').scope().listarArquivos();
               //recarregar pagina
           },
        });
      }             

    });

    $("#selecao-arquivo").change(function(e){

        var data = new FormData();
        $.each(e.target.files, function(key, value){
            data.append(key, value);
        });
        
        $.ajax({
            url: 'gerenciar-arquivo.php?param=cadastrar-arquivos',
            type: 'POST',
            data: data,
            success: function (data) {
             angular.element('#myCtrl').scope().listarArquivos();
         },
         cache: false,
         contentType: false,
         processData: false,
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                    myXhr.upload.addEventListener('progress', function () {
                        // faz alguma coisa durante o progresso do upload 
                    }, false);
                }
                return myXhr;
            }
        });

    });

    $("#fechar").click(function(){
        $(".alert-mudar").css( "display", "none" );
    });

    $(".rodape .gerar-pacote").click(function(){

        var data = new FormData();
        data.append("nome_pacote", $(".rodape #nome-pacote").val());
        data.append("versao_banco", $(".rodape #nome-versao-banco").val());
        data.append("versao_reversao", $(".rodape #nome-versao-reversao").val());

        $(this).val("Gerando pacote ...");
        $(this).prop( "disabled", true );

        $.ajax({
            url: 'gerenciar-arquivo.php?param=zippar-arquivo',
            type: 'POST',
            data: data,
            success: function (data) {

                $.ajax({
                    url: 'gerenciar-arquivo.php?param=deletar-todos-arquivos',
                    success: function (data) {
                     angular.element('#myCtrl').scope().listarArquivos();
                           //recarregar pagina
                       },
                   }); 

                $.ajax({
                    url: 'gerenciar-arquivo.php?param=deletar-arquivo-informata-pack',
                    success: function (data) {
                     angular.element('#myCtrl').scope().listarArquivos();
                           //recarregar pagina
                       },
                   }); 

                // "out/meus pacotes/'+$(".rodape #nome-pacote").val()+'_'+$(".rodape #nome-versao-banco").val()+'.zip "
                $(".rodape .gerar-pacote").val("Gerar pacote");
                $(".rodape .gerar-pacote").prop( "disabled", false );
                $(".baixar-file-pack").html("<a href='out/meus pacotes/"+$(".rodape #nome-pacote").val()+"_"+$(".rodape #nome-versao-banco").val()+".zip '><input type='button' value='Baixar "+$(".rodape #nome-pacote").val()+"_"+$(".rodape #nome-versao-banco").val()+".zip ' id='gerar-pacote' class='voltar-botao-pack'>");
                $(".voltar-botao-pack").click(function(){
                  $(".baixar-file-pack").html("<input type='button' value='Gerar pacote' id='gerar-pacote' class='gerar-pacote'>");
                });
                $(".rodape #nome-pacote").val("");
                $(".rodape #nome-versao-banco").val("");
                $(".rodape #nome-versao-reversao").val("");

            },
            cache: false,
            contentType: false,
            processData: false,
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                    myXhr.upload.addEventListener('progress', function () {
                        // faz alguma coisa durante o progresso do upload 
                    }, false);
                }
                return myXhr;
            }
        }); 

    });



    //---- MODIFICANDO PAPEL PAREDE
    //----------------------------------------------------------------------------------    
    $(".rodape #nome-pacote").change(function(){
      if($(".rodape #nome-pacote").val() == "matrix"){
        $(".total").css("background-image", "url('img/matrix.gif')");
      }
      if($(".rodape #nome-pacote").val() == "jean"){
        $(".total").css("background-image", "url('img/jean.png')");
      }
      if($(".rodape #nome-pacote").val() == "fatality"){
        $(".total").css("background-image", "url('img/fatality.gif')");
      }      
    });


});

    //---- ANGULAR JS
    //----------------------------------------------------------------------------------    
    var app = angular.module('myApp', ['ngSanitize']);
    
    app.controller('myCtrl', function($scope, $http) {

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=infomdlog")
      .then(function(response) {
          $scope.listarArquivosMdlog    = response.data;
          console.log(response)
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=dbamdata")
      .then(function(response) {
          $scope.listarArquivosDbamdata = response.data;
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=reversao")
      .then(function(response) {
          $scope.listarArquivosReversao = response.data;
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=reversao-dba")
      .then(function(response) {
          $scope.listarArquivosReversaoDba = response.data;
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-reversao-dba-script")
      .then(function(response) {
          $scope.listarArquivosRevDbamdataScripts = response.data;
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-banco-dba-script")
      .then(function(response) {
          $scope.listarArquivosDbamdataScripts = response.data;
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-banco-dba-grant")
      .then(function(response) {
          $scope.listarArquivosDbamdataGrant = response.data;
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-reversao-dba-grant")
      .then(function(response) {
          $scope.listarArquivosReversaoDbamdataGrant = response.data;
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-banco-dba-trigger")
      .then(function(response) {
          $scope.listarArquivosDbamdataTrigger = response.data;
      });

      $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-reversao-dba-trigger")
      .then(function(response) {
          $scope.listarArquivosRevDbamdataTrigger = response.data;
      });

      $scope.listarArquivos = function(){

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=infomdlog")
          .then(function(response) {
              $scope.listarArquivosMdlog    = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=dbamdata")
          .then(function(response) {
              $scope.listarArquivosDbamdata = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=reversao")
          .then(function(response) {
              $scope.listarArquivosReversao = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=reversao-dba")
          .then(function(response) {
              $scope.listarArquivosReversaoDba = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-reversao-dba-script")
          .then(function(response) {
              $scope.listarArquivosRevDbamdataScripts = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-banco-dba-script")
          .then(function(response) {
              $scope.listarArquivosDbamdataScripts = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-banco-dba-grant")
          .then(function(response) {
              $scope.listarArquivosDbamdataGrant = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-reversao-dba-grant")
          .then(function(response) {
              $scope.listarArquivosReversaoDbamdataGrant = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-banco-dba-trigger")
          .then(function(response) {
              $scope.listarArquivosDbamdataTrigger = response.data;
          });

          $http.get("gerenciar-arquivo.php?param=listar-arquivos&tipo-lista=lista-reversao-dba-trigger")
          .then(function(response) {
              $scope.listarArquivosRevDbamdataTrigger = response.data;
          });

      };

  }); 

