<?php 
	error_reporting(0); 
	//var_dump($_SERVER["REMOTE_ADDR"]);
	//$_COOKIE["user"] = $_SERVER["REMOTE_ADDR"];
	//var_dump($_COOKIE);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Criptografador de pacotes informata</title>

	<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
	<link href="lib/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
	<link href="lib/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">

	<script src="lib/js/jquery-1.11.3.min.js"></script>	
	<script src="lib/js/angular.min.js"></script>	
	<script src="lib/js/angular-sanitize.js"></script>	
	<script src="lib/js/mask/jquery.mask.js"></script>	
	<script src="js/index.js"></script>	

</head>
<body>

<?php  

if($_GET['opAlterar'])
{

	echo "

		<div class='alert-mudar'>

		<div class='topo-mudar'>
			<font id='fechar'>X</font>
			<font id='titulo'>Escolha a nova posição do arquivo! </font>
		</div>

		<div class='conteudo'>
			<form method='post' action='gerenciar-arquivo.php?param=mover-arquivo'>
				<input type='hidden' name='diretorio-arquivo' value='{$_GET['diretorio']}'>
				<input type='hidden' name='arquivo' value='{$_GET['arquivo']}'>
				<font> banco > infomdlog: <input type='radio' name='escolha' value='upload/banco/infomdlog/'></font>
				<font> banco > dbamdata: <input type='radio' name='escolha' value='upload/banco/dbamdata/'></font>

				<font> banco > dbamdata > trigger: <input type='radio' name='escolha' 	value='in/upload/banco/dbamdata/trigger/'></font>
				<font> banco > dbamdata > grant: <input type='radio' name='escolha' 	value='in/upload/banco/dbamdata/grant/'></font>
				<font> banco > dbamdata > scripts: <input type='radio' name='escolha' 	value='in/upload/banco/dbamdata/scripts/'></font>

				<font> reversao > infomdlog: <input type='radio' name='escolha' value='in/upload/reversao/infomdlog/'></font>
				<font> reversao > dbamdata: <input type='radio' name='escolha' value='in/upload/reversao/dbamdata/'></font>

				<font> reversao > dbamdata > trigger: <input type='radio' name='escolha' 	value='in/upload/reversao/dbamdata/trigger/'></font>
				<font> reversao > dbamdata > grant: <input type='radio' name='escolha' 		value='in/upload/reversao/dbamdata/grant/'></font>
				<font> reversao > dbamdata > scripts: <input type='radio' name='escolha' 	value='in/upload/reversao/dbamdata/scripts/'></font>

				<input type='submit' value='Modificar' id='submit'>
			</form>
		</div>

		</div>

	";

}

?>
<div class="modal-gerar-pacote">
	<font class='fechar-modal'>X</font>
	<font class="titulo" >Gerador de pacotes informata!</font>
    <input type="text" name="nome_pacote" placeholder="Digite o nome do produto!" id="nome-pacote"  maxlength="25">
    <input type="text" name="versao_banco" 	placeholder="Release Aplicação" id="nome-versao-banco"  maxlength="25" title="Digite a versão do release há ser aplicada no banco!">
    <input type="text" name="versao_reversao" placeholder="Release Reversão" id="nome-versao-reversao"  maxlength="25" title="Digite a versão do release de reversão!">
    <div class="baixar-file">
		<input type="button" value="Gerar pacote" id="gerar-pacote">
	</div>
	<font class="observacao">Obs.: o backup do pacote gerado estará na maquina 192.168.0.232 em E:/app/Monta_Pacote/(ano)/(mes)/(dia)/(nome do projeto)_(release da aplicacao) </font>
</div>

<div class="total" ng-app="myApp" ng-controller="myCtrl" id="myCtrl">

	<div class="topo">
		<a href="lib/game/index.html"><img src="img/logo.png" id="logo"></a>
		<form id="formulario" name="formulario" method="post" enctype="multipart/form-data">
			<label for='selecao-arquivo' id="subir-arquivo">upload via arquivo</label>
			<input id='selecao-arquivo' type='file' name="arquivo[]" multiple >
			<label id="subir-arquivo-tortoise">upload via tortoise</label>
		</form>

		<div id="urlRepo" style="display:none">
		    <input type="text" name="nome_pacote" placeholder="Digite a url do repositorio tortoise!" class="input-repo">
		    <i class="fa fa-times esconderUrlRepo" aria-hidden="true"></i>
		    <!-- svn log -v -r 0:N --limit 100 [--stop-on-copy] PATH -->
		</div>

		<div id="menu-top">
			<i class="fa fa-archive icone-duvida-informativo abrir-modal fa-2x" aria-hidden="true"></i>
		</div>
	</div>

	<div class="conteudo">

		<font id="titulo">Banco</font>

		<font id="titulo">Reversao</font> 
		<div class="banco">
			<font id="titulo-banco">PROCEDURE</font>
			<font id="titulo-banco">SCRIPT</font>
			<div class="subpastas">		
				<font ng-bind-html="listarArquivosMdlog"></font>
			</div>
			<div class="subpastas">
				<div id='listar-arquivo-dba'>
					<font id='subtitulo-dbamdata'>Raiz</font>
					<font ng-bind-html="listarArquivosDbamdata"></font>
				</div>
				<div id='listar-arquivo-scripts-dba'>
					<font id='subtitulo-dbamdata'>Trigger</font>
					<font ng-bind-html="listarArquivosDbamdataTrigger"></font>
				</div>
				<div id='listar-arquivo-scripts-dba'>
					<font id='subtitulo-dbamdata'>Grant</font>
					<font ng-bind-html="listarArquivosDbamdataGrant"></font>
				</div>
				<div id='listar-arquivo-scripts-dba'>
					<font id='subtitulo-dbamdata'>Scripts</font>
					<font ng-bind-html="listarArquivosDbamdataScripts"></font>
				</div>
			</div>
		</div>
		<div class="banco">
			<font id="titulo-banco">PROCEDURE</font>
			<font id="titulo-banco">SCRIPT</font>
			<div class="subpastas">		
				<font ng-bind-html="listarArquivosReversao"></font>		
			</div>
			<div class="subpastas">
				<div id='listar-arquivo-dba'>
					<font id='subtitulo-dbamdata'>Raiz</font>				
					<font ng-bind-html="listarArquivosReversaoDba"></font>
				</div>
				<div id='listar-arquivo-scripts-dba'>
					<font id='subtitulo-dbamdata'>Trigger</font>
					<font ng-bind-html="listarArquivosRevDbamdataTrigger"></font>
				</div>			
				<div id='listar-arquivo-scripts-dba'>
					<font id='subtitulo-dbamdata'>Grant</font>
					<font ng-bind-html="listarArquivosReversaoDbamdataGrant"></font>
				</div>			
				<div id='listar-arquivo-scripts-dba'>
					<font id='subtitulo-dbamdata'>Scripts</font>
					<font ng-bind-html="listarArquivosRevDbamdataScripts"></font>
				</div>			
			</div>
		</div>

		<div class="bottom-conteudo">
			<i class="fa fa-trash fa-2x excluir-todos-arquivos" aria-hidden="true"></i>
		</div>

	</div>


	<div class="rodape">
		<form>
		    <input type="text" name="nome_pacote"  placeholder="Digite o nome do produto!"  id="nome-pacote" class="nome-pacote">
		    <input type="text" name="versao_banco"  placeholder="Release Aplicação" id="nome-versao-banco" class="nome-versao-banco" title="Digite a versão do release há ser aplicada no banco!">
		    <input type="text" name="versao_reversao"  placeholder="Release Reversão" id="nome-versao-reversao" title="Digite a versão do release de reversão!" class="nome-versao-reversao">
			<div class="baixar-file-pack">
				<input type="button" value="Gerar pacote" id="gerar-pacote" class="gerar-pacote" disabled="" style="background: #999;opacity: 0.5;">
			</div>
		</form>
	</div>
</div>
</body>
</html>
