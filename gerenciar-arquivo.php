<?php 

include "variaveis.php";
include "bd_connection.php";

if($_GET['param'] == "cadastrar-arquivos"){

	// DITERÓRIOS 
	$caminho_banco_dbamdata  	= $PATH_FILES."banco/dbamdata/";
	$caminho_banco_dba_scripts  = $PATH_FILES."banco/dbamdata/scripts/";
	$caminho_banco_dba_trigger  = $PATH_FILES."banco/dbamdata/trigger/";
	$caminho_banco_dba_grant    = $PATH_FILES."banco/dbamdata/grant/";
	$caminho_banco_infomlog 	= $PATH_FILES."banco/infomdlog/";
	$caminho_reversao_infomlog  = $PATH_FILES."reversao/infomdlog/";
	$caminho_reversao_dbamdata  = $PATH_FILES."reversao/dbamdata/";
	$caminho_reversao_dba_scrpt = $PATH_FILES."reversao/dbamdata/scripts/";
	$caminho_reversao_dba_trgr  = $PATH_FILES."reversao/dbamdata/trigger/";
	$caminho_reversao_dba_grant = $PATH_FILES."reversao/dbamdata/grant/";

	// PADRÕES
	$str_titulo_reversao 		= "R_";

	foreach($_FILES as $file)
	{

		// validar se o arquivo é txt ou sql.
		if(	$file['type'] == "text/plain" OR $file['type'] == "application/octet-stream")
		{

		    $arquivo_temporario = $file['tmp_name'];
		    $caminho_arquivo  	= "";

		    // abrir o arquivo
		    $ponteiro 			= fopen($arquivo_temporario, "r");
			$le_arquivo 		= fread($ponteiro, filesize($arquivo_temporario));

		    // buscar padrões nos arquivos
			$bol_reversao_titulo 	= strpos(substr(strtoupper($file['name']),0,2),$str_titulo_reversao);
			$bol_grant_titulo 	 	= strpos(strtoupper($file['name']),"GRANT");			
			$bol_numero_titulo 		= filter_var($file['name'], FILTER_SANITIZE_NUMBER_INT);
			$bol_existe_insert 		= strripos(strtoupper($le_arquivo),"INSERT");
			$bol_existe_update 		= strripos(strtoupper($le_arquivo),"UPDATE");
			$bol_existe_delete 		= strripos(strtoupper($le_arquivo),"DELETE");
			$bol_existe_select 		= strripos(strtoupper($le_arquivo),"SELECT");
			$bol_existe_grant 		= strripos(strtoupper($le_arquivo),"GRANT");
			$bol_existe_trigger 	= strripos(strtoupper($le_arquivo),"TRIGGER");
			$bol_existe_create	 	= strripos(strtoupper($le_arquivo),"CREATE");
			$bol_existe_after 	 	= strripos(strtoupper($le_arquivo),"AFTER");
			$bol_existe_create_pub 	= strripos(strtoupper($le_arquivo),"CREATE PUBLIC");

		    // direcionar cada arquivo para o caminho certo
			// se o titulo for reversao
			IF($bol_reversao_titulo !== FALSE)
			{
				// não houver numero no titulo
				IF(($bol_numero_titulo == ''))
				{
			   		$caminho_arquivo 	 = $caminho_reversao_infomlog;
				}ELSE
				{

					$caminho_arquivo = $caminho_reversao_dbamdata;
					
					IF(($bol_existe_insert !== FALSE) OR ($bol_existe_update !== FALSE) OR ($bol_existe_delete !== FALSE) OR ($bol_existe_select !== FALSE))
					{
						$caminho_arquivo = $caminho_reversao_dba_scrpt;
					}
					IF(($bol_existe_grant !== FALSE))
					{
						$caminho_arquivo = $caminho_reversao_dba_grant;
					}
					IF(($bol_existe_trigger !== FALSE))
					{
						$caminho_arquivo = $caminho_reversao_dba_trgr;
					}
					IF(($bol_existe_create !== FALSE) OR ($bol_existe_after !== FALSE) AND !($bol_existe_create_pub !== FALSE))
					{
						$caminho_arquivo = $caminho_reversao_dbamdata;	
					}
					IF($bol_grant_titulo !== FALSE)
					{
						$caminho_arquivo = $caminho_reversao_dba_grant;	
					}

				}	
			}ELSE
			{
				// não houver numero no titulo
				IF(($bol_numero_titulo == ''))
				{
				   	$caminho_arquivo 	 = $caminho_banco_infomlog;
				}ELSE
				{

					$caminho_arquivo = $caminho_banco_dbamdata;

					IF(($bol_existe_insert !== FALSE) OR ($bol_existe_update !== FALSE) OR ($bol_existe_delete !== FALSE) OR ($bol_existe_select !== FALSE))
					{
						$caminho_arquivo = $caminho_banco_dba_scripts;
					}
					IF(($bol_existe_grant !== FALSE))
					{
						$caminho_arquivo = $caminho_banco_dba_grant;
					}
					IF(($bol_existe_trigger !== FALSE))
					{
						$caminho_arquivo = $caminho_banco_dba_trigger;
					}
					IF(($bol_existe_create !== FALSE) OR ($bol_existe_after !== FALSE) AND !($bol_existe_create_pub !== FALSE))
					{
						$caminho_arquivo = $caminho_banco_dbamdata;	
					}					
					IF($bol_grant_titulo !== FALSE)
					{
						$caminho_arquivo = $caminho_banco_dba_grant;	
					}	

				}	
			}

			move_uploaded_file($arquivo_temporario, $caminho_arquivo.$file['name']);
		}

	}

}

if($_GET['param'] == "listar-arquivos"){

	$path = $PATH_FILES;
	$obj  = new stdClass();

	if($_GET['tipo-lista'] == "infomdlog")
		$path .= "banco/infomdlog/";
	else if($_GET['tipo-lista'] == "dbamdata")
		$path .= "banco/dbamdata/";
	else if($_GET['tipo-lista'] == "reversao-dba")
		$path .= "reversao/dbamdata/";		
	else if($_GET['tipo-lista'] == "lista-banco-dba-script")
		$path .= "banco/dbamdata/scripts/";		
	else if($_GET['tipo-lista'] == "lista-reversao-dba-script")
		$path .= "reversao/dbamdata/scripts/";		
	else if($_GET['tipo-lista'] == "lista-banco-dba-grant")
		$path .= "banco/dbamdata/grant/";		
	else if($_GET['tipo-lista'] == "lista-reversao-dba-grant")
		$path .= "reversao/dbamdata/grant/";		
	else if($_GET['tipo-lista'] == "lista-banco-dba-trigger")
		$path .= "banco/dbamdata/trigger/";		
	else if($_GET['tipo-lista'] == "lista-reversao-dba-trigger")
		$path .= "reversao/dbamdata/trigger/";		
	else
		$path .= "reversao/infomdlog/";

	$diretorio = dir($path);
	$diretorio->read();
	$diretorio->read();

	while($arquivo = $diretorio->read()){

		if($arquivo != "scripts" AND $arquivo != "trigger" AND $arquivo != "grant"){
			echo "<div class='item-arquivo'>
					<div class='rodape-item-arquivo'> 
						<span>	
							$arquivo
						</span> 
					    <a href='?opAlterar=true&diretorio={$path}{$arquivo}&arquivo={$arquivo}'  class='link'>
					    	<i class='fa fa-exchange' aria-hidden='true' title='Mover o arquivo'></i>
					    </a>
					    <a href='gerenciar-arquivo.php?param=ler-arquivo&local={$path}{$arquivo}' class='link' target='_blank'>
					    	<i class='fa fa-file-code-o' aria-hidden='true' title='ler o arquivo'></i>
					    </a>
						<a href='gerenciar-arquivo.php?param=excluir-arquivo&arq={$path}{$arquivo}'  class='link'>
							<i class='fa fa-trash-o' aria-hidden='true' title='Deletar o arquivo'></i>
						</a>
					</div> 
				  </div>";

		}
	}

	$diretorio -> close();
}

if($_GET['param'] == "excluir-arquivo"){

	if (unlink($_GET['arq']))
		header("Location:index.php");

}

if($_GET['param'] == "mover-arquivo"){

	// copiar o arquivo para o local desejado
	copy($_POST['diretorio-arquivo'], $_POST['escolha'].$_POST['arquivo']);
	
	// excluir o arquivo anterior
	if (unlink($_POST['diretorio-arquivo']))
		header("Location:index.php");	
}


if($_GET['param'] == "zippar-arquivo"){

	// validando qual será o nome do pacote
	if(empty($_POST['criptografar']) and strlen($_POST['nome_pacote']) > 0){
		$pack_name = $_POST['nome_pacote'];
		$zipfile   = "out/meus pacotes/".$pack_name."_".$_POST['versao_banco'].".zip";
	}else{
		$pack_name = md5(date('mm:hh:ss'));
		$zipfile   = "out/meus pacotes/".$pack_name."_".$_POST['versao_banco'].".zip";
	}
    
	// ------------------------------------------------------------------------------------------------------ 
	// ------------------------------------ ENCRIPTOGRAFANDO OS ARQUIVOS ------------------------------------ 
	// ------------------------------------------------------------------------------------------------------ 

	// ------------------------- CAMINHO ------------------------- 
	$path_infomdlog 	 = $PATH_FILES."banco/infomdlog/";
	$path_dbamdata  	 = $PATH_FILES."banco/dbamdata/";
	$path_scr_dba   	 = $PATH_FILES."banco/dbamdata/scripts/";
	$path_dba_grant 	 = $PATH_FILES."banco/dbamdata/grant/";
	$path_dba_trigger    = $PATH_FILES."banco/dbamdata/trigger/";
	$path_reversao  	 = $PATH_FILES."reversao/infomdlog/";
	$path_rev_dba   	 = $PATH_FILES."reversao/dbamdata/";
	$path_rev_dba_scr    = $PATH_FILES."reversao/dbamdata/scripts/";
	$path_reb_dba_tri    = $PATH_FILES."reversao/dbamdata/trigger/";
	$path_rev_dba_grant  = $PATH_FILES."reversao/dbamdata/grant/";

	$path_cript_infomdlog 	 	= $PATH_FLE_CRI."banco/infomdlog/";
	$path_cript_dbamdata  	 	= $PATH_FLE_CRI."banco/dbamdata/";
	$path_cript_scr_dba   	 	= $PATH_FLE_CRI."banco/dbamdata/scripts/";
	$path_cript_dba_grant 	 	= $PATH_FLE_CRI."banco/dbamdata/grant/";
	$path_cript_dba_trigger    	= $PATH_FLE_CRI."banco/dbamdata/trigger/";
	$path_cript_reversao  	 	= $PATH_FLE_CRI."reversao/infomdlog/";
	$path_cript_rev_dba   	 	= $PATH_FLE_CRI."reversao/dbamdata/";
	$path_cript_rev_dba_scr    	= $PATH_FLE_CRI."reversao/dbamdata/scripts/";
	$path_cript_reb_dba_tri    	= $PATH_FLE_CRI."reversao/dbamdata/trigger/";
	$path_cript_rev_dba_grant  	= $PATH_FLE_CRI."reversao/dbamdata/grant/";

	putenv("ORACLE_HOME=".$PATH_WRAP);

	// --------------------- LENDO DIRETORIO --------------------- 
	$diretorio_infomdlog = dir($path_infomdlog);
	$diretorio_dbamdata  = dir($path_dbamdata);
	$diretorio_reversao  = dir($path_reversao);
	$diretorio_rev_dba   = dir($path_rev_dba);
	$diretorio_scr_dba   = dir($path_scr_dba);
	$dire_rev_dba_scr    = dir($path_rev_dba_scr);
	$dire_dba_trigger    = dir($path_dba_trigger);
	$dire_rev_dba_tri    = dir($path_reb_dba_tri);
	$dire_dba_grant      = dir($path_dba_grant);
	$dire_rev_dba_grant  = dir($path_rev_dba_grant);	

	$cript_diretorio_infomdlog = dir($path_cript_infomdlog);
	$cript_diretorio_dbamdata  = dir($path_cript_dbamdata);
	$cript_diretorio_reversao  = dir($path_cript_reversao);
	$cript_diretorio_rev_dba   = dir($path_cript_rev_dba);
	$cript_diretorio_scr_dba   = dir($path_cript_scr_dba);
	$cript_dire_rev_dba_scr    = dir($path_cript_rev_dba_scr);
	$cript_dire_dba_trigger    = dir($path_cript_dba_trigger);
	$cript_dire_rev_dba_tri    = dir($path_cript_reb_dba_tri);
	$cript_dire_dba_grant      = dir($path_cript_dba_grant);
	$cript_dire_rev_dba_grant  = dir($path_cript_rev_dba_grant);

	// ---------------------- REMOVENDO LIXO --------------------- 
	$diretorio_infomdlog->read();$diretorio_infomdlog->read();
	$diretorio_dbamdata->read()	;$diretorio_dbamdata->read();
//	$diretorio_dbamdata->read()	;$diretorio_dbamdata->read(); $diretorio_dbamdata->read();
	$diretorio_reversao->read()	;$diretorio_reversao->read();
	$diretorio_rev_dba->read()	;$diretorio_rev_dba->read()	;
	$diretorio_rev_dba->read()	;$diretorio_rev_dba->read()	; $diretorio_rev_dba->read();
	$diretorio_scr_dba->read()	;$diretorio_scr_dba->read()	;
	$dire_rev_dba_scr->read()	;$dire_rev_dba_scr->read()	;
	$dire_dba_trigger->read()	;$dire_dba_trigger->read()	;
	$dire_rev_dba_tri->read()	;$dire_rev_dba_tri->read()	;
	$dire_dba_grant->read()		;$dire_dba_grant->read()	;
	$dire_rev_dba_grant->read()	;$dire_rev_dba_grant->read();

	$cript_diretorio_infomdlog->read()  ;$cript_diretorio_infomdlog->read()	;
	$cript_diretorio_dbamdata->read()	;$cript_diretorio_dbamdata->read()	;
	$cript_diretorio_dbamdata->read()	;$cript_diretorio_dbamdata->read()	; $cript_diretorio_dbamdata->read();
	$cript_diretorio_reversao->read()	;$cript_diretorio_reversao->read()	;
	$cript_diretorio_rev_dba->read()	;$cript_diretorio_rev_dba->read()	;
	$cript_diretorio_rev_dba->read()	;$cript_diretorio_rev_dba->read()	; $cript_diretorio_rev_dba->read();
	$cript_diretorio_scr_dba->read()	;$cript_diretorio_scr_dba->read()	;
	$cript_dire_rev_dba_scr->read() 	;$cript_dire_rev_dba_scr->read()	;
	$cript_dire_dba_trigger->read()		;$cript_dire_dba_trigger->read()	;
	$cript_dire_rev_dba_tri->read()		;$cript_dire_rev_dba_tri->read()	;
	$cript_dire_dba_grant->read()		;$cript_dire_dba_grant->read()		;
	$cript_dire_rev_dba_grant->read()	;$cript_dire_rev_dba_grant->read()	;




	// ---------------- MOVENDO ARQUIVOS CRIPTOGRAFADOS E DELETANDO ARQUIVOS NAO CRIPTOGRAFADOS ---------------- 
	while($arquivo = $diretorio_infomdlog->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_infomdlog.$arquivo." oname=".$PATH_PROJECT.$path_cript_infomdlog.$arquivo,$return);
		//unlink($path_infomdlog.$arquivo);
	}

	while($arquivo = $diretorio_dbamdata->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_dbamdata.$arquivo." oname=".$PATH_PROJECT.$path_cript_dbamdata.$arquivo,$return);
		//unlink($path_dbamdata.$arquivo);
	}

	while($arquivo = $diretorio_reversao->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_reversao.$arquivo." oname=".$PATH_PROJECT.$path_cript_reversao.$arquivo,$return);
		//unlink($path_reversao.$arquivo);
	}

	while($arquivo = $diretorio_rev_dba->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_rev_dba.$arquivo." oname=".$PATH_PROJECT.$path_cript_rev_dba.$arquivo,$return);
		//unlink($path_rev_dba.$arquivo);
	}

	while($arquivo = $diretorio_scr_dba->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_scr_dba.$arquivo." oname=".$PATH_PROJECT.$path_cript_scr_dba.$arquivo,$return);
		//unlink($path_scr_dba.$arquivo);
	}

	while($arquivo = $dire_rev_dba_scr->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_rev_dba_scr.$arquivo." oname=".$PATH_PROJECT.$path_cript_rev_dba_scr.$arquivo,$return);
		//unlink($path_rev_dba_scr.$arquivo);
	}

	while($arquivo = $dire_dba_trigger->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_dba_trigger.$arquivo." oname=".$PATH_PROJECT.$path_cript_dba_trigger.$arquivo,$return);
		//unlink($path_dba_trigger.$arquivo);
	}

	while($arquivo = $dire_rev_dba_tri->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_reb_dba_tri.$arquivo." oname=".$PATH_PROJECT.$path_cript_reb_dba_tri.$arquivo,$return);
		//unlink($path_reb_dba_tri.$arquivo);
	}

	while($arquivo = $dire_dba_grant->read()){
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_dba_grant.$arquivo." oname=".$PATH_PROJECT.$path_cript_dba_grant.$arquivo,$return);
		//unlink($path_dba_grant.$arquivo);
	}

	while($arquivo = $dire_rev_dba_grant->read()){		
		system($PATH_WRAP."\\BIN\\wrap iname=".$PATH_PROJECT.$path_rev_dba_grant.$arquivo." oname=".$PATH_PROJECT.$path_cript_rev_dba_grant.$arquivo,$return);
		//unlink($path_rev_dba_grant.$arquivo);
	}

	// ------------------------------------------------------------------------------------------------------ 
	// --------------------------------- CODIGO SQL PARA GERAR OS PACOTES ----------------------------------- 
	// ------------------------------------------------------------------------------------------------------ 

	try{

	    $stmt = $conn->exec("
							begin
						    dbagabos.montagem_pacotes.monta_pacote('".$DIRE_FL_PACK."',
																	  '".$_POST['versao_banco']."',
																	  '".$_POST['versao_reversao']."', 
																	  'PRJ',
																	  'NORMAL',
																	  'S');
							end;
							");

    } catch (Exception $e) {
        echo "Erro: Código: " . $e->getCode() . " Mensagem: " . $e->getMessage();
    }

	// ------------------------------------------------------------------------------------------------------ 
	// ---------------------------------------- PROCESSO DE ZIPAGEM ----------------------------------------- 
	// ------------------------------------------------------------------------------------------------------ 

	// processo de zippagem
	$directory = $PATH_FLE_CRI;
    $filenames = array();
    function browse($dir) {
    global $filenames;
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && is_file($dir.'/'.$file)) {
                    $filenames[] = $dir.'/'.$file;
                }
                else if ($file != "." && $file != ".." && is_dir($dir.'/'.$file)) {
                    browse($dir.'/'.$file);
                }
            }
            closedir($handle);
        }
        return $filenames;
    }

    browse($directory);
    $zip = new ZipArchive();
    if ($zip->open($zipfile, ZIPARCHIVE::CREATE)!==TRUE) {
        exit("Não pode abrir: <$zipfile>\n");
    }
    foreach ($filenames as $filename) {
        echo "Arquivo adicionado: <b>" . $filename . "<br/></b>";
        $zip->addFile($filename,$filename);
    }
    echo "Total de arquivos: <b>" . $zip->numFiles . "</b>\n";
    //header ("Location: ".$zipfile);

	// -------------- FECHANDO LEITURA ARQUIVO --------------- 
	$diretorio_infomdlog -> close();
	$diretorio_reversao  -> close();
	$diretorio_rev_dba   -> close();
	$diretorio_scr_dba   -> close();
	$dire_rev_dba_scr    -> close();
	$dire_dba_trigger    -> close();
	$dire_rev_dba_tri    -> close();
	$dire_dba_grant      -> close();
	$dire_rev_dba_grant  -> close();

}

if($_GET['param'] == "deletar-todos-arquivos")
{

	$caminho_infomdlog = $PATH_FILES."banco/infomdlog/";
	$caminho_dbamdata  = $PATH_FILES."banco/dbamdata/";
	$caminho_reversao  = $PATH_FILES."reversao/infomdlog/";
	$caminho_rev_dba   = $PATH_FILES."reversao/dbamdata/";
	$caminho_scr_dba   = $PATH_FILES."banco/dbamdata/scripts/";
	$cam_rev_dba_scr   = $PATH_FILES."reversao/dbamdata/scripts/";
	$cam_dba_trigger   = $PATH_FILES."banco/dbamdata/trigger/";
	$cam_reb_dba_tri   = $PATH_FILES."reversao/dbamdata/trigger/";
	$cam_dba_grant 	   = $PATH_FILES."banco/dbamdata/grant/";
	$cam_rev_dba_grant = $PATH_FILES."reversao/dbamdata/grant/";

	$diretorio_infomdlog = dir($caminho_infomdlog);
	$diretorio_dbamdata  = dir($caminho_dbamdata);
	$diretorio_reversao  = dir($caminho_reversao);
	$diretorio_rev_dba   = dir($caminho_rev_dba);
	$diretorio_scr_dba   = dir($caminho_scr_dba);
	$dire_rev_dba_scr    = dir($cam_rev_dba_scr);
	$dire_dba_trigger    = dir($cam_dba_trigger);
	$dire_rev_dba_tri    = dir($cam_reb_dba_tri);
	$dire_dba_grant      = dir($cam_dba_grant);
	$dire_rev_dba_grant  = dir($cam_rev_dba_grant);
	
	while($arquivo = $diretorio_infomdlog->read()){
		unlink($caminho_infomdlog."".$arquivo);
	}

	while($arquivo2 = $diretorio_dbamdata->read()){
		unlink($caminho_dbamdata."".$arquivo2);
	}

	while($arquivo3 = $diretorio_reversao->read()){
		unlink($caminho_reversao."".$arquivo3);
	}

	while($arquivo4 = $diretorio_rev_dba->read()){
		unlink($caminho_rev_dba."".$arquivo4);
	}

	while($arquivo5 = $diretorio_scr_dba->read()){
		unlink($caminho_scr_dba."".$arquivo5);
	}

	while($arquivo6 = $dire_rev_dba_scr->read()){
		unlink($cam_rev_dba_scr."".$arquivo6);
	}

	while($arquivo7 = $dire_dba_trigger->read()){
		unlink($cam_dba_trigger."".$arquivo7);
	}

	while($arquivo8 = $dire_rev_dba_tri->read()){
		unlink($cam_reb_dba_tri."".$arquivo8);
	}

	while($arquivo9 = $dire_dba_grant->read()){
		unlink($cam_dba_grant."".$arquivo9);
	}

	while($arquivo10 = $dire_rev_dba_grant->read()){
		unlink($cam_rev_dba_grant."".$arquivo10);
	}

}

if($_GET['param'] == "ler-arquivo")
{

	$arquivo = fopen($_GET['local'],"r");

	while( !feof($arquivo)){

		$linha = fgets($arquivo);
		echo $linha ."<br>";

	}

	fclose($arquivo);

}

if($_GET['param'] == "deletar-arquivo-informata-pack")
{

	$path_cript_infomdlog 	 	= $PATH_FLE_CRI."banco/infomdlog/";
	$path_cript_dbamdata  	 	= $PATH_FLE_CRI."banco/dbamdata/";
	$path_cript_scr_dba   	 	= $PATH_FLE_CRI."banco/dbamdata/scripts/";
	$path_cript_dba_grant 	 	= $PATH_FLE_CRI."banco/dbamdata/grant/";
	$path_cript_dba_trigger    	= $PATH_FLE_CRI."banco/dbamdata/trigger/";
	$path_cript_reversao  	 	= $PATH_FLE_CRI."reversao/infomdlog/";
	$path_cript_rev_dba   	 	= $PATH_FLE_CRI."reversao/dbamdata/";
	$path_cript_rev_dba_scr    	= $PATH_FLE_CRI."reversao/dbamdata/scripts/";
	$path_cript_reb_dba_tri    	= $PATH_FLE_CRI."reversao/dbamdata/trigger/";
	$path_cript_rev_dba_grant  	= $PATH_FLE_CRI."reversao/dbamdata/grant/";

	$path_informata_pack  		= $PATH_FLE_CRI;
	$path_informata_pack_rvrs  	= $PATH_FLE_CRI."reversao/";

	$cript_diretorio_infomdlog = dir($path_cript_infomdlog);
	$cript_diretorio_dbamdata  = dir($path_cript_dbamdata);
	$cript_diretorio_reversao  = dir($path_cript_reversao);
	$cript_diretorio_rev_dba   = dir($path_cript_rev_dba);
	$cript_diretorio_scr_dba   = dir($path_cript_scr_dba);
	$cript_dire_rev_dba_scr    = dir($path_cript_rev_dba_scr);
	$cript_dire_dba_trigger    = dir($path_cript_dba_trigger);
	$cript_dire_rev_dba_tri    = dir($path_cript_reb_dba_tri);
	$cript_dire_dba_grant      = dir($path_cript_dba_grant);
	$cript_dire_rev_dba_grant  = dir($path_cript_rev_dba_grant);

	$cript_informata_pack      = dir($path_informata_pack);
	$cript_informata_pack_rvrs = dir($path_informata_pack_rvrs);

	while($arquivo = $cript_diretorio_infomdlog->read()){
		unlink($path_cript_infomdlog.$arquivo);
	}

	while($arquivo = $cript_diretorio_dbamdata->read()){
		unlink($path_cript_dbamdata.$arquivo);
	}

	while($arquivo = $cript_diretorio_reversao->read()){
		unlink($path_cript_reversao.$arquivo);
	}

	while($arquivo = $cript_diretorio_rev_dba->read()){
		unlink($path_cript_rev_dba.$arquivo);
	}

	while($arquivo = $cript_diretorio_scr_dba->read()){
		unlink($path_cript_scr_dba.$arquivo);
	}

	while($arquivo = $cript_dire_rev_dba_scr->read()){
		unlink($path_cript_rev_dba_scr.$arquivo);
	}

	while($arquivo = $cript_dire_dba_trigger->read()){
		unlink($path_cript_dba_trigger.$arquivo);
	}

	while($arquivo = $cript_dire_rev_dba_tri->read()){
		unlink($path_cript_reb_dba_tri.$arquivo);
	}

	while($arquivo = $cript_dire_dba_grant->read()){
		unlink($path_cript_dba_grant.$arquivo);
	}

	while($arquivo = $cript_dire_rev_dba_grant->read()){		
		unlink($path_cript_rev_dba_grant.$arquivo);
	}

	while($arquivo = $cript_informata_pack->read()){		
		unlink($path_informata_pack.$arquivo);
	}

	while($arquivo = $cript_informata_pack_rvrs->read()){		
		unlink($path_informata_pack_rvrs.$arquivo);
	}

}

if($_GET['param'] == "gerar-pasta-pacote"){

	// criar pasta do nome do pacote
	$diretorio_pack = "E:\\app\\Monta_Pacote\\";
	
	// $diretorio_pack = "pacote_testeeee\\";

	if(!file_exists($diretorio_pack)){
		mkdir($diretorio_pack, 0777); 
	}

	$diretorio_pack = $diretorio_pack.date('Y')."\\";

	if(!file_exists($diretorio_pack)){
		mkdir($diretorio_pack, 0777); 
	}

	$diretorio_pack = $diretorio_pack.date('m')."\\";

	if(!file_exists($diretorio_pack)){
		mkdir($diretorio_pack, 0777); 
	}

	$diretorio_pack = $diretorio_pack.date('d')."\\";

	if(!file_exists($diretorio_pack)){
		mkdir($diretorio_pack, 0777); 
	}

	$directorio_pacote = $diretorio_pack.$_POST['nome_pacote']."_".$_POST['versao_banco']."\\";

	mkdir($directorio_pacote, 0777);
	mkdir($directorio_pacote."banco\\", 0777);
	mkdir($directorio_pacote."reversao\\", 0777);
	mkdir($directorio_pacote."reversao\\banco\\", 0777);

	try{

		$sql = "begin
				   dbagabos.montagem_pacotes.monta_pacote('".$directorio_pacote."',
			                                              '".$_POST['versao_banco']."',
			                                              '".$_POST['versao_reversao']."', 
			                                              'PRJ',
			                                              'NORMAL',
			                                              'S');
				end;
				";

		$stmt = $conn->exec($sql);

		/*
		-- ZIPPANDO O ARQUIVO
		---------------------------------------------------
		*/

		$zipfile   = "out/meus pacotes dinamicos/".$_POST['nome_pacote']."_".$_POST['versao_banco'].".zip";
		$directory = $directorio_pacote;
	    $filenames = array();
	    function browse($dir) {
	    global $filenames;
	        if ($handle = opendir($dir)) {
	            while (false !== ($file = readdir($handle))) {
	                if ($file != "." && $file != ".." && is_file($dir.'/'.$file)) {
	                    $filenames[] = $dir.'/'.$file;
	                }
	                else if ($file != "." && $file != ".." && is_dir($dir.'/'.$file)) {
	                    browse($dir.'/'.$file);
	                }
	            }
	            closedir($handle);
	        }
	        return $filenames;
	    }

	    browse($directory);
	    $zip = new ZipArchive();
	    if ($zip->open($zipfile, ZIPARCHIVE::CREATE)!==TRUE) {
	        exit("Não pode abrir: <$zipfile>\n");
	    }
	    foreach ($filenames as $filename) {
	        echo "Arquivo adicionado: <b>" . $filename . "<br/></b>";
	        $zip->addFile($filename,$filename);
	    }
	    echo "Total de arquivos: <b>" . $zip->numFiles . "</b>\n";
	    //header ("Location: ".$zipfile);		

    } catch (Exception $e) {
        echo "Erro: Código: " . $e->getCode() . " Mensagem: " . $e->getMessage();
    }

}

if($_GET['param'] == "gerar-arq-por-repo"){
	//var_dump(svn_log('https://192.168.0.235:8443/svn/IMdlog/branches/71.10.00.00/Idatabase'));
	// svn log -v -r 0:N --limit 100 [--stop-on-copy] https://192.168.0.235:8443/svn/IMdlog/branches/71.10.00.00/Idatabase 
	// var_dump($svn);
	system("svn log https://192.168.0.235:8443/svn/IMdlog/branches/71.10.00.00/Idatabase");
}	

?>