/********************************************************************************
  Autor     : Gerailton Benigno
  CRC       : 74461
  Data      : 24/01/2018
  Descricao : Criado novo par�metro para informar a vis�o geral do estoque.
********************************************************************************/

BEGIN
  INSERT INTO DBAWMS.TW091_CONTROLE_ACESSO (TW091_ACESSO_IU, TW091_DESCRICAO)
   VALUES ('0604', 'Vis�o Geral do Estoque');
  COMMIT;
END;
/
