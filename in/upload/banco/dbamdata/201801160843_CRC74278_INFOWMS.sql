--Schema   : INFOWMS
--Analista : Fernando Camargo
--Data     : 16/01/2018
--CRC      : 74278

ALTER TABLE INFOWMS.WTEMP_MONITORACAO_CONTAGEM ADD (WTEMP_LOCAL           NUMBER(2),
                                                    WTEMP_DESCRICAO_LOCAL VARCHAR2(30),
                                                    WTEMP_RUA             VARCHAR2(10),  
                                                    WTEMP_TIPO_ENDERECO   CHAR(1),
                                                    WTEMP_DESCRICAO_TIPO  VARCHAR2(10));