-- WRAP LAB
----------------------------------------------

	É um projeto desenvolvido pela informata


-- ORGANIZAÇÃO DO PROJETO
----------------------------------------------

	wrap_lab/
		|
		+--css/
		+--img/
		+--js/
		|	|
		|	+--index.js
		|	+--gerarpacotes.js
		|
		+--lib/
		+--in/
		|	|
		|	+--upload/
		|	+--upload_cripto/
		|
		+--bd_connection.php
		+--gerarpacotes.php
		+--gerenciararquivo.php
		+--index.php
		+--variaveis.php

	-> in/upload/ : todos os arquivos que foram exibidos 
	  no index.php estão nesta pasta, ao gerar o pacote,
	  todos os arquivos que estiverem dentro da  pasta 
	  upload serão criptografados na pasta in/informata_pack/.

	-> in/upload_cripto/ : todos os arquivos dessa pasta
	   são temporarios eles vem do in/upload/ são 
	   criptografados e logo apos zipado em out/meus pacotes/.	

	-> bd_connection.php : Responsável pela conexão
	   oracle da aplicação.

	-> gerarpacotes.php : Tela que irá gerar pacotes
	   de banco e reversão. seus dados serão gerados

	-> index.php : Tela responsavel por gerar pacote
	   dos arquivos que foram selecionados pelo tortoise
	   ou por arquivo


-- CONTRIBUIÇÃO
----------------------------------------------

	João Neto (joao.neto@informata.com.br)
	Jean Nascimento (jean.nascimento@informata.com.br)